db.fruits.aggregate([

    {$match:{price:{$lt:50},supplier:"Yellow Farms"}},
    {$count:"QtyYFItemsPrice<50"}

])


db.fruits.aggregate([

    {$match:{price:{$lt:30}}},
    {$count:"QtyAllItemsPrice<30"}

])


db.fruits.aggregate([

    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"YellowFarmsAvgPrice",avgPrice:{$avg:"$price"}}}

])


db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"RedFarmsHighestPrice",highestPrice:{$max:"$price"}}}

])


db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"RedFarmsLowestPrice",lowestPrice:{$min:"$price"}}}

])