db.fruits.insertMany(
    [
        {
            name:"Apple",
            supplier:"Red Farms Inc.",
            stocks:20,
            price:40,
            onSale:true
        },
        {
            name:"Banana",
            supplier:"Yellow Farms",
            stocks:15,
            price:20,
            onSale:true // changed to false during discussion
        },
        {
            name:"Kiwi",
            supplier:"Green Farming and Canning",
            stocks:25,
            price:50,
            onSale:true
        },
        {
            name:"Mango",
            supplier:"Yellow Farms",
            stocks:10,
            price:60,
            onSale:true
        },
        {
            name:"Dragon Fruit",
            supplier:"Red Farms Inc.",
            stocks:10,
            price:60,
            onSale:true
        }
    ]
)

/*
	Aggregation Pipeline Stages

	Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage

	$match - this is used to match or get documents which satisfies the condition
				syntax: {$match: {field:<value>}}
	
	$group - allows us to group together documents and create an analysis out of the grouped documents
		always use _id: assume that you are always creating a temporary document
		_id: in the group stage, essentially associates an id to our results
		_id: also determines the number of groups
		_id: "$supplier" - essentially group together documents with the same values in the supplier field.
		_id: $<field>

*/

db.fruits.aggregate([

	// looked for and got all fruits that are onSale
    {$match: {onSale:true}},

    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}

])


db.fruits.aggregate([

	// if the _id's value is definite or given, $group will only create one group
	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	// if the _id's value is definite or given, $group will only create one group
	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"RedFarmsInc.",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true,supplier:"Yellow Farms"}},
	// or {$match: {$and: [{onSale:true},{supplier:"Yellow Farms"}]}},
	{$group: {_id:"YellowFarmsOnSale",totalStocks:{$sum:"$stocks"}}}

])


// $avg - is an operator used in $group stage
// $avg get s the average of the numerical values of the indicated field in grouped documents

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier",avgStock:{$avg:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,avgPrice:{$avg:"$price"}}}

]) // result: 52.50


// $max - will allow us to get the highest value out of all the value in a given field per group


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"HighestStockOnSale",maxStock:{$max:"$stocks"}}}

]) // result: 25 (highest stock on sale)


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,maxPrice:{$max:"$price"}}}

]) // result: 60 (highest price on sale)


// $min - gets the lowest value of the values in a given field per group


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"LowestStockOnSale",minStock:{$min:"$stocks"}}}

]) // result: 10 (lowest stock on sale)


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"LowestPriceOnSale",minPrice:{$min:"$price"}}}

]) // result: 40 (lowest price on sale)


db.fruits.aggregate([

	{$match: {price:{$lt:50}}},
	{$group: {_id:"LowestStock",lowestStock:{$min:"$stocks"}}}

]) // result: 15 (lowest stock for items with price less than 50)


// $count - is a stage added after $match stage to count all items that matches our criteria


db.fruit.aggregate([

	{$match: {onSale:true}},
	{$count:"itemsOnSale"}

]) // result: 4 (counted all items on sales)


db.fruits.aggregate([

	{$count:"itemsOnSale"}

]) // result: 5 (counted all items on sale and not on sale)


db.fruits.aggregate([

	{$match:{price:{$lt:50}}},
	{$count:"itemsPriceLessThan50"}

]) // result: 2 (counted all items whose price is less than 50)


db.fruits.aggregate([

	{$match:{stocks:{$lt:20}}},
	{$count:"itemsStocksLessThan20"}

]) // result: 3 (counted all items whose stocks are less than 20)


// $out - save the results in a new collection. However, this will overwrite the collection if it already exists

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
	{$out:"stocksPerSupplier"}

])